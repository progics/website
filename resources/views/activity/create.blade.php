@extends('layouts.app')

@section('title', 'Home')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h3>Maak een activiteit aan</h3>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <form method="POST" action="{{ route('store-activity') }}">
                                    @csrf
                                    @method('POST')
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="mb-3">
                                                <label for="title" class="form-label">Titel:</label>
                                                <input type="text" class="form-control" id="title" name="title"
                                                       placeholder="Voeg een titel toe...">
                                            </div>
                                            <div class="mb-3">
                                                <label for="description" class="form-label">Omschrijving:</label>
                                                <textarea class="description" name="description"
                                                          id="description"></textarea>
                                                <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                                                <script>
                                                    tinymce.init({
                                                        selector: 'textarea.description',
                                                        height: 500
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="date" class="form-label">Datum:</label>
                                                <input type="date" class="form-control" id="date" name="date">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="time" class="form-label">Tijd:</label>
                                                <input type="time" class="form-control" id="time" name="time">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="location" class="form-label">Locatie:</label>
                                                <select class="form-select" aria-label="Default select example" id="location" name="location">
                                                    <option selected>Open this select menu</option>
                                                    <option value="1">De Brink</option>
                                                    <option value="2">De Es</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" type="submit">Submit form</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
