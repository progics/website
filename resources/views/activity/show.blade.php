@extends('layouts.app')

@section('title', 'PIOEMEL')

@section('content')

    @csrf
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ $activity->title }}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{ \Carbon\Carbon::parse($activity->created_at)->format('d-m-Y')}}</h6>
                        <p class="card-text">{!! $activity->description !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <p class="font-weight-bolder">Algemene informatie</p>
                        <p class="mb-n1 font-weight-bold">Datum & Tijd:</p>
                        <p class="mt-2">{{ \Carbon\Carbon::parse($activity->date)->format('d-m-Y')}}
                            om {{ \Carbon\Carbon::parse($activity->time)->format('H:i')}}</p>
                        <p class="mb-n1 font-weight-bold">Locatie:</p>
                        <p class="mt-2">{{ $activity->location->name }}</p>
                        <p class="mb-n1 font-weight-bold">Bestanden:</p>
                        <a href="" class="mt-2">Opgaveformulier.docx</a>
                        <br>
                        <a href="" class="mt-2">flyerbingo.docx</a>
                        <hr>
                        <p class="mb-n1 font-weight-bold">Georganiseerd door:</p>
                        <p class="mt-2">{{ $activity->user->firstname }} {{ $activity->user->lastname }}</p>
                    </div>
                </div>
            </div>

            <div class="col-md-12 mb-n4 mt-5">
                <p>Deelnemers:</p>
            </div>
            @foreach($registrations as $registration)
                <div class="col-md-3 mt-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-10 col-11">
                                    <h5 class="card-title">{{ $registration->resident->firstname }} {{ $registration->resident->lastname }}</h5>
{{--                                    <h6 class="card-subtitle mb-2 text-muted">{{ $registration->resident->room_id->room->name }}</h6>--}}
                                </div>
                                <div class="col-md-2 col-1 text-center">
                                    <i class="fas fa-credit-card"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
