@extends('layouts.app')

@section('title', 'Home')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10">
                Alle activiteiten:
            </div>
            <div class="col-md-2 text-center">
                <a data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="add-icon"><i
                        class="fas fa-plus"></i></a>
            </div>
            @foreach($activities as $activity)
                <div class="col-md-6">
                    <a href="{{ route('activity.show', [$activity->id]) }}">
                        <div class="card mt-2 activity-link">
                            <div class="card-body">
                                <h5 class="card-title">{{ $activity->title }}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">{{ \Carbon\Carbon::parse($activity->date)->format('d-m-Y')}}
                                    om {{ \Carbon\Carbon::parse($activity->time)->format('H:i')}}</h6>
                                <div class="card-text">{!! Str::limit($activity->description, 300) !!}</div>
                                <h6 class="card-link">{{ $activity->location->name }}</h6>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>

    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
         aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Maak een activiteit aan...</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('store.activity') }}">
                        @csrf
                        @method('POST')
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-3">
                                    <label for="title" class="form-label">Titel:</label>
                                    <input type="text" class="form-control" id="title" name="title"
                                           placeholder="Voeg een titel toe...">
                                </div>
                                <div class="mb-3">
                                    <label for="description" class="form-label">Omschrijving:</label>
                                    <textarea class="description" name="description"
                                              id="description"></textarea>
                                    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                                    <script>
                                        tinymce.init({
                                            selector: 'textarea.description',
                                            height: 500
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="date" class="form-label">Datum:</label>
                                    <input type="date" class="form-control" id="date" name="date">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="time" class="form-label">Tijd:</label>
                                    <input type="time" class="form-control" id="time" name="time">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="location_id" class="form-label">Locatie:</label>
                                    <select class="form-select" aria-label="Default select example" id="location_id"
                                            name="location_id">
                                        <option selected>Selecteer een locatie...</option>
                                        @foreach($locations as $location)
                                            <option value="{{ $location->id }}">{{ $location->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-6 d-flex align-items-center me-auto">
                                <button class="btn btn-primary" type="submit">Opslaan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
