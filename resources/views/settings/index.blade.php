@extends('layouts.app')

@section('title', 'Home')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#" onclick="openPage(event, 'profile')"
                           id="defaultOpen">Profiel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="openPage(event, 'workers')">Medewerkers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="openPage(event, 'locations')">Locaties</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="openPage(event, 'residents')">Bewoners</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="openPage(event, 'rooms')">Woningen</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">
                <div id="profile" class="tabcontent">
                    <h3>Profiel</h3>
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('update-user', [Auth::user()->id]) }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label for="firstname" class="form-label">Voornaam:*</label>
                                            <input type="text" class="form-control" id="firstname" name="firstname"
                                                   value="{{ Auth::user()->firstname }}">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label for="lastname" class="form-label">Achternaam:*</label>
                                            <input type="text" class="form-control" id="lastname" name="lastname"
                                                   value="{{ Auth::user()->lastname }}">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label for="zipcode" class="form-label">E-mail:*</label>
                                            <input type="email" class="form-control" id="email" name="email"
                                                   value="{{ Auth::user()->email }}">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label for="phone" class="form-label">Telefoon:*</label>
                                            <input type="tel" class="form-control" id="phone" name="phone"
                                                   value="{{ Auth::user()->phone }}">
                                        </div>
                                    </div>
                                    <div class="col-12 text-right">
                                        <button class="btn btn-primary" type="submit">Opslaan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="workers" class="tabcontent">
                    <div class="row">
                        <div class="col-10">
                            <h3>Medewerkers</h3>
                        </div>
                        <div class="col-2 text-center">
                            @if(Auth::user()->role_id == 3)
                                <a data-bs-toggle="modal" data-bs-target="#workermodal" class="add-icon"><i class="fas fa-plus"></i></a>
                            @endif
                        </div>
                        <div class="col-12">
                            <p>Hier vindt u alle medewerkers. U heeft hier ook de mogelijkheid om een medewerker toe
                                te
                                voegen,
                                te bewerken en te verwijderen.</p>
                        </div>
                        @foreach($users as $user)
                            <div class="col-6 col-md-3">
                                <div class="card mt-2">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $user->firstname }} {{ $user->lastname }}</h5>
                                        <h6 class="card-subtitle mb-2 text-muted">{{ $user->role->title }}</h6>
                                        <p class="card-text">{{ $user->email }}
                                            <br>
                                            {{ $user->phone }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div id="locations" class="tabcontent">
                    <div class="row">
                        <div class="col-10">
                            <h3>Locaties</h3>
                        </div>
                        <div class="col-2 text-center">
                            @if(Auth::user()->role_id == 3)
                                <a data-bs-toggle="modal" data-bs-target="#locationsmodal" class="add-icon"><i
                                        class="fas fa-plus"></i></a>
                            @endif
                        </div>
                        <div class="col-12">
                            <p>Hier vindt u alle locaties. U heeft hier ook de mogelijkheid om een locatie toe te
                                voegen, te
                                bewerken en te verwijderen.</p>
                        </div>
                        @foreach($locations as $location)
                            <div class="col-md-6">
                                <div class="card mt-2">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $location->name }}</h5>
                                        <h6 class="card-subtitle mb-2 text-muted">{{ $location->street }}
                                            <br> {{ $location->zipcode }}, {{ $location->city }}</h6>
                                        <h6 class="card-link">{{ $location->phone }}</h6>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div id="residents" class="tabcontent">
                    <div class="row">
                        <div class="col-10">
                            <h3>Bewoners</h3>
                        </div>
                        <div class="col-2 text-center">
                            @if(Auth::user()->role_id == 3)
                                <a data-bs-toggle="modal" data-bs-target="#residentmodal" class="add-icon"><i
                                        class="fas fa-plus"></i></a>
                            @endif
                        </div>
                        <div class="col-12">
                            <p>Hier vindt u alle bewoners. U heeft hier ook de mogelijkheid om een bewoner toe te
                                voegen, te
                                bewerken en te verwijderen.</p>
                        </div>
                        @foreach($residents as $resident)
                            <div class="col-md-3">
                                <div class="card mt-2">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-10 col-11">
                                                <h5 class="card-title">{{ $resident->firstname }} {{ $resident->lastname }}</h5>
                                                <h6 class="card-subtitle mb-2 text-muted">{{ $resident->client_id }}</h6>
                                                <h6 class="card-link">Kamer {{ $resident->room->name }}
                                                    <br>
                                                    {{ $resident->room->location->name }}
                                                </h6>
                                            </div>
                                            @if($resident->card == 1)
                                                <div class="col-md-2 col-1 text-center">
                                                    <i class="fas fa-credit-card"></i>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div id="rooms" class="tabcontent">
                    <div class="row">
                        <div class="col-10">
                            <h3>Woningen</h3>
                        </div>
                        <div class="col-2 text-center">
                            @if(Auth::user()->role_id == 3)
                                <a data-bs-toggle="modal" data-bs-target="#roommodal" class="add-icon"><i class="fas fa-plus"></i></a>
                            @endif
                        </div>
                        <div class="col-12">
                            <p>Hier vindt u alle woningen. U heeft hier ook de mogelijkheid om een woning toe te
                                voegen,
                                te
                                bewerken en te verwijderen.</p>
                        </div>
                        @foreach($rooms as $room)
                            <div class="col-6 col-md-3">
                                <div class="card mt-2">
                                    <div class="card-body">
                                        <h5 class="card-title">Kamer {{ $room->name }}</h5>
                                        <h6 class="card-subtitle mb-2 text-muted">{{ $room->location->name }}</h6>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function openPage(evt, pageName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the link that opened the tab
            document.getElementById(pageName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        document.getElementById("defaultOpen").click();
    </script>

    @include('includes.settings-modals')

@endsection
