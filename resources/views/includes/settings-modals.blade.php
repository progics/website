<!-- Modal add location -->
<div class="modal fade" id="locationsmodal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Voeg een locatie toe...</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('store-location') }}">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="name" class="form-label">Naam:*</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="Voeg een naam toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="street" class="form-label">Straat + Huisnummer:*</label>
                                <input type="text" class="form-control" id="street" name="street"
                                       placeholder="Voeg een straat toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="zipcode" class="form-label">Postcode:*</label>
                                <input type="text" class="form-control" id="zipcode" name="zipcode"
                                       placeholder="Voeg een postcode toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="city" class="form-label">Stad:*</label>
                                <input type="text" class="form-control" id="city" name="city"
                                       placeholder="Voeg een stad toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="phone" class="form-label">Telefoon:*</label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       placeholder="Voeg een telegfoon toe..." required>
                            </div>
                            <button class="btn btn-primary" type="submit">Opslaan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal add worker -->
<div class="modal fade" id="workermodal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Voeg een medewerker toe...</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('store-worker') }}">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="firstname" class="form-label">Voornaam:*</label>
                                <input type="text" class="form-control" id="firstname" name="firstname"
                                       placeholder="Voeg een voornaam toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="lastname" class="form-label">Achternaam:*</label>
                                <input type="text" class="form-control" id="lastname" name="lastname"
                                       placeholder="Voeg een achternaam toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="zipcode" class="form-label">E-mail:*</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Voeg een email toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="phone" class="form-label">Telefoon:*</label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       placeholder="Voeg een telefoonnummer toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="role" class="form-label">Functie:*</label>
                                <select class="form-select" aria-label="Default select example" id="role"
                                        name="role_id">
                                    <option selected>Selecteer een functie...</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-primary" type="submit">Opslaan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal add room -->
<div class="modal fade" id="roommodal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Voeg een kamer toe...</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('store-room') }}">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="name" class="form-label">Kamer-naam:*</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="Voeg een kamer-naam toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="location" class="form-label">Locatie:*</label>
                                <select class="form-select" aria-label="Default select example" id="location"
                                        name="location">
                                    <option selected>Selecteer een locatie...</option>
                                    @foreach($locations as $location)
                                        <option value="{{ $location->id }}">{{ $location->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-primary" type="submit">Opslaan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal add resident -->
<div class="modal fade" id="residentmodal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Voeg een bewoner toe...</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('store-resident') }}">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="client_id" class="form-label">Client-id:*</label>
                                <input type="text" class="form-control" id="client_id" name="client_id"
                                       placeholder="Voeg een client-id toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="salutation" class="form-label">Aanhef:*</label>
                                <select class="form-select" aria-label="Default select example" id="salutation"
                                        name="salutation">
                                    <option selected>Selecteer een optie...</option>
                                    <option value="Meneer,">Meneer,</option>
                                    <option value="Mevrouw,">Mevrouw,</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="firstname" class="form-label">Voornaam:*</label>
                                <input type="text" class="form-control" id="firstname" name="firstname"
                                       placeholder="Voeg een voornaam toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="lastname" class="form-label">Achternaam:*</label>
                                <input type="text" class="form-control" id="lastname" name="lastname"
                                       placeholder="Voeg een achternaam toe..." required>
                            </div>
                            <div class="mb-3">
                                <label for="room_id" class="form-label">Kamer:*</label>
                                <select class="form-select" aria-label="Default select example" id="room_id"
                                        name="room_id">
                                    <option selected>Selecteer een kamer...</option>
                                    @foreach($rooms as $room)
                                        <option value="{{ $room->id }}">Kamer {{ $room->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="card" class="form-label">Heeft de bewoner een Zinn-pas:*</label>
                                <select class="form-select" aria-label="Default select example" id="card"
                                        name="card">
                                    <option selected>Selecteer een optie...</option>
                                        <option value="0">Nee</option>
                                        <option value="1">Ja</option>
                                </select>
                            </div>
                            <button class="btn btn-primary" type="submit">Opslaan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
