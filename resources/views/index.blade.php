@extends('layouts.app')

@section('title', 'Home')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-n4">
                <h3>Welkom, <b>{{ Auth::user()->firstname }}</b></h3>
            </div>
            <div class="col-md-12 mt-5 mb-n3">
                <p>Mijn locaties:</p>
            </div>
            @foreach($locations as $location)
                <div class="col-md-6">
                    <div class="card mt-2">
                        <div class="card-body">
                            <h5 class="card-title">{{ $location->name }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $location->street }} <br> {{ $location->zipcode }}, {{ $location->city }}</h6>
                            <h6 class="card-link">{{ $location->phone }}</h6>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-md-12 mt-5 mb-n3">
                <p>Mijn activiteiten:</p>
            </div>
            @foreach($activities as $activity)
                <div class="col-md-6">
                    <a href="{{ route('activity.show', [$activity->id]) }}">
                        <div class="card mt-2 activity-link">
                            <div class="card-body">
                                <h5 class="card-title">{{ $activity->title }}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">{{ \Carbon\Carbon::parse($activity->date)->format('d-m-Y')}}
                                    om {{ \Carbon\Carbon::parse($activity->time)->format('H:i')}}</h6>
                                <p class="card-text">{!! Str::limit($activity->description, 300) !!}</p>
                                <h6 class="card-link">{{ $activity->location->name }}</h6>
                            </div>
                        </div>
                    </a>
                </div>
        @endforeach
        <div class="col-md-12 text-center">
            <a href="{{ route('activity') }}">Alle activiteiten...</a>
        </div>

    </div>
    </div>

@endsection
