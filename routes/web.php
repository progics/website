<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\SettingsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', [Controller::class, 'index'])->name('index')->middleware('auth');

Route::get('/activiteiten', [ActivityController::class, 'index'])->name('activity')->middleware('auth');
Route::get('/activiteiten/{id}', [ActivityController::class, 'show'])->name('activity.show')->middleware('auth');
Route::get('/activiteiten/aanmaken', [ActivityController::class, 'create'])->name('create.activity')->middleware('auth');
Route::post('/activiteiten/aanmaken', [ActivityController::class, 'store'])->name('store.activity')->middleware('auth');
//Route::get('/activiteiten/{id}/edit', [ActivityController::class, 'edit'])->name('edit-activity')->middleware('auth');
//Route::get('/activiteiten/{id}/delete', [ActivityController::class, 'delete'])->name('delete-activity')->middleware('auth');

Route::get('/instellingen', [SettingsController::class, 'index'])->name('settings')->middleware('auth');
Route::post('/instellingen/aanmaken/locatie', [SettingsController::class, 'store_location'])->name('store-location')->middleware('auth');
Route::post('/instellingen/aanmaken/medewerker', [SettingsController::class, 'store_worker'])->name('store-worker')->middleware('auth');
Route::post('/instellingen/aanmaken/kamer', [SettingsController::class, 'store_room'])->name('store-room')->middleware('auth');
Route::post('/instellingen/aanmaken/bewoner', [SettingsController::class, 'store_resident'])->name('store-resident')->middleware('auth');
Route::put('/instellingen/{id}/bewerken', [SettingsController::class, 'update_user'])->name('update-user')->middleware('auth');
