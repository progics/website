<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Room;
use App\Models\Resident;
use App\Models\Location;
use App\Models\Role;

class SettingsController extends Controller
{
    public function index()
    {

        $users = User::orderBy('firstname')->get();
        $rooms = Room::orderBy('name')->get();
        $residents = Resident::orderBy('firstname')->get();
        $locations = Location::orderBy('name')->get();
        $roles = Role::all();

        return view('settings.index', compact('users', 'rooms', 'residents', 'locations', 'roles'));
    }

    public function store_location()
    {

        $location = new Location();
        $location->name = request('name');
        $location->street = request('street');
        $location->city = request('city');
        $location->zipcode = request('zipcode');
        $location->phone = request('phone');

        $location->save();

        return back();

    }

    public function store_worker() {

        $worker = new User();
        $worker->firstname = request('firstname');
        $worker->lastname = request('lastname');
        $worker->email = request('email');
        $worker->phone = request('phone');
        $worker->role_id = request('role_id');
        $worker->password = '$2y$10$XjrLsk0YWP1vOrUOXdAj5e1hRob/tuSjxuwnsG2lX0sffgxQXmytq';

        $worker->save();

        return back();
    }

    public function store_room() {

        $room = new Room();
        $room->name = request('name');
        $room->location_id = request('location');

        $room->save();

        return back();
    }

    public function store_resident() {

        $resident = new Resident();
        $resident->client_id = request('client_id');
        $resident->firstname = request('firstname');
        $resident->lastname = request('lastname');
        $resident->salutation = request('salutation');
        $resident->room_id = request('room_id');
        $resident->card = request('card');

        $resident->save();

        return back();
    }

    public function update_user($id) {

        $user = User::find($id);

        $user->firstname = request('firstname');
        $user->lastname = request('lastname');
        $user->email = request('email');
        $user->phone = request('phone');

        $user->save();

        return back();
    }
}
