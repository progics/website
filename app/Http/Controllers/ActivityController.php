<?php

namespace App\Http\Controllers;

use App\Models\Registration;
use Illuminate\Http\Request;
use App\Models\Activity;
use Illuminate\Support\Facades\Auth;
use App\Models\Location;
use App\Models\User;

class ActivityController extends Controller
{
    public function index() {

        $activities = Activity::all();
        $locations = Location::all();

        return view('activity.index', compact('activities', 'locations'));
    }

    public function show($id) {

        $activity = Activity::find($id);
        $registrations = Registration::where('activity_id', $id)->get();

        return view('activity.show', compact('activity', 'registrations'));
    }

    public function create() {



        return view('activity.create', compact('locations'));
    }

    public function store() {

        $activity = new Activity();
        $activity->title = request('title');
        $activity->description = request('description');
        $activity->date = request('date');
        $activity->time = request('time');
        $activity->location_id = request('location_id');
        $activity->organised_by = Auth::user()->id;

        $activity->save();

        return back();

    }

    public function edit() {

    }

    public function delete() {

    }
}
