<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    public function activity() {
        return $this->hasMany(Activity::class);
    }

    public function room() {
        return $this->hasMany(Room::class);
    }
}
