<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'organised_by');
    }

    public function registration() {
        return $this->hasMany(Registration::class);
    }
}
