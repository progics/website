<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resident extends Model
{
    use HasFactory;

    public function registration() {
        return $this->hasMany(Registration::class);
    }

    public function room() {
        return $this->belongsTo(Room::class, 'room_id');
    }
}
